# Overview
This library holds a generic set of LTRAC related libraries for dealing with HDF5 files. Currently (July 2016) the only important thing it holds is `h5todict.py`

  + **h5todict.py:** provides a function for turning a HDF5 file into a python dict, skipping data by default, and converting all values to native serializable Python types. It was developed to server the specific needs of the web development group at the time and may not be generally useful, and may be better replaced with a more general robust method later.
  + **h5dumpers/:** contains a couple of HDF5 to <some-human-readable-format> dumpers, that are probably not generally useful. They were developed to test h5todict and learn, and left hanging about in the ltrach5lib.

# Requirements

  * h5py
  * python3
