import sys
import os
import h5py
import numpy
from os import path
import hashlib


def h5todict(obj, datasetHandlers=[], context={}, depth=0):
    '''
    Converts a h5's meta data into a *simplified* dict representation. Production rules roughly:
            <h5file> ::- <group>
            <group> ::- {
                type: "group",
                attributes: {},
                groups: [<group>+],
                datasets: [<dataset>],
                <dataset-inbuilt-attributes>
            }
            <dataset> ::- {
                type: "dataset",
                attributes: {},
                <dataset-inbuilt-attributes>
            }
    Groups and datasets are separated for convenience.
    By default don't include dataset data because they're generally big.
    If you want to include dataset data you pass a handler for the data.
    The handler is passed the dataset dict and should put data in "value"
    prop and mutate the dataset props however it wants.
    If a list of handlers is provided, each is tried until one returns true.
    @todo Improve this. This is a bit of a hacky parser implementation but does the job...
    @todo Cant figure how to get a stringified H5 ref out of obejcts from h5py. So using digest of name.
    '''
    typeStr = None
    if isinstance(obj, h5py.Group):
        typeStr = "group"
    elif isinstance(obj, h5py.Dataset):
        typeStr = "dataset"
    if typeStr is None:
        raise TypeError("Expected h5py.Group or h5py.Dataset found " + obj)
    if isinstance(obj, h5py.Dataset) and depth == 0:
        raise TypeError("Expected h5py.Group found " + obj)
    if type(datasetHandlers) is not list:  # Try to convert if only one handler passed in.
        datasetHandlers = [datasetHandlers]
    context['type'] = typeStr
    context['name'] = obj.name
    context['id'] = hashlib.sha1(obj.name.encode()).hexdigest()  # Would prefer h5 ref..
    context['attributes'] = {}
    for attr in obj.attrs:
        context['attributes'][attr] = h5GetAttr(obj.attrs[attr])
    if isinstance(obj, h5py.Group):
        context['groups'] = {}
        context['datasets'] = {}
        for item in obj:
            if isinstance(obj[item], h5py.Group):
                context['groups'][item] = {}
                h5todict(obj[item], datasetHandlers, context['groups'][item], depth+1)
            elif isinstance(obj[item], h5py.Dataset):
                context['datasets'][item] = {}
                h5todict(obj[item], datasetHandlers, context['datasets'][item], depth+1)
    else:  # isinstance(obj, h5py.Dataset):
        context['ndim'] = len(obj.shape)
        context['shape'] = obj.shape
        context['dtype'] = str(obj.dtype)
        context['len'] = obj.len() if context['ndim'] else 0  # Scalar dsets raise on len() (they've 0 length).
        for handler in datasetHandlers:
            if handler(obj, context, depth):
                break
    return context


def h5GetAttr(attr):
    '''
    There is some weird ways of encoding a scalar about.
    We are not that worried about maintaining type, so try to guess if they meant `scalar`.
    '''
    _attr = None
    if isinstance(attr, (int, float, bool, str)):
        _attr = attr
    else:
        try:
            if isinstance(attr, numpy.ndarray) and len(attr) != 1:
                _attr = attr.tolist()
            else:
                _attr = attr.item()
            if isinstance(_attr, bytes):
                _attr = _attr.decode("utf-8")
        except (ValueError, AttributeError) as e:
            pass
    return _attr


def h5GetAttrs(attrs):
    _attrs = {}
    for k, v in attrs.items():
        _attrs[k] = h5GetAttr(v)
    return _attrs


def dumpDataHandler(obj, context, depth):
    '''
    Example data handler.
    '''
    context['value'] = obj.value.tolist()


def nullDataHandler(obj, context, depth):
    '''
    Example data handler
    '''
    pass
