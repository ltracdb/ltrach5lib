from setuptools import setup, find_packages
import sys
import os


version = '0.1'
setup(
    name='ltrach5lib',
    version=version,
    description="",
    classifiers=[
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.4',
    ],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Sam Pinkus',
    author_email='sgpin1@student.monash.edu',
    license='',
    packages=['ltrach5lib'],
    package_dir={'ltrach5lib': '.'},
    include_package_data=True,
    zip_safe=False,
    install_requires=['numpy', 'h5py'],
)
