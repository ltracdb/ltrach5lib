#!/usr/bin/env python
'''
Test script to extract and save all images from a h5 file.
This script is not used or useful for anything.
'''
import sys
import os
import h5py
import numpy as np
import Image
import logging
from os import path
from pprint import pprint


def extractimages(h5File):
    h5 = h5py.File(h5File, "r")
    h5images = findimages(h5)
    for h5image in h5images:
        imgpath = "./" + h5image.name + ".png"
        imgdir = os.path.dirname(imgpath)
        logging.debug(imgpath, imgdir)
        if(not os.path.isdir(imgdir)):
            os.makedirs("./" + imgdir)
        colormodel = 'RGB'
        if 'IMAGE_COLORMODEL' in h5image.attrs.keys():
            colormodel = h5image.attrs['IMAGE_COLORMODEL']
        img = Image.new(colorModel, (obj.shape[1], obj.shape[0]))
        objData = obj[:].tolist()  # Faster.
        # @todo ineff. Use putdata().
        for i in range(0, obj.shape[0]):  # Y
            for j in range(0, obj.shape[1]):  # X
                pixel = tuple(objData[i][j])
                img.putpixel((j, i), pixel)
        img.save(imgpath)


def findimages(h5):
    imageList = _findimages(h5)
    return imageList


def _findimages(obj, imageList=[]):
    '''
    Find all images.
    Note currently only support IMAGE_SUBCLASS=IMAGE_TRUECOLOR, INTERLACE_MODE=INTERLACE_PIXEL
    This seems to be what hdfview generates.
    See http://www.hdfgroup.org/HDF5/doc/ADGuide/ImageSpec.html
    '''
    if isinstance(obj, h5py.Group):
        for child in obj:
            _findimages(obj[child], imageList)
    elif isinstance(obj, h5py.Dataset):
        if 'CLASS' in obj.attrs.keys() and obj.attrs['CLASS'] == 'IMAGE':  # h5py attrs does not have has_key() - PoS.
            if not len(obj.shape) == 3:
                logging.warning("Skipping unsupported image format. \
                    All supported formats have dim=3. Found dim=" + len(obj.shape))
            elif 'IMAGE_SUBCLASS' in obj.attrs.keys() and obj.attrs['IMAGE_SUBCLASS'] != 'IMAGE_TRUECOLOR':
                logging.warning("Skipping unsupported image format. Require IMAGE_SUBCLASS=IMAGE_TRUECOLOR")
            elif 'INTERLACE_MODE' in obj.attrs.keys() and obj.attrs['INTERLACE_MODE'] != 'INTERLACE_PIXEL':
                logging.warning("Skipping unsupported image format. Require INTERLACE_MODE=INTERLACE_PIXEL")
            elif 'PALETTE' in obj.attrs.keys():
                logging.warning("Skipping unsupported image format. PALETTE not currently supported")
            else:
                imageList.append(obj)
    else:
        raise TypeError(type(obj))
    return imageList


if len(sys.argv) != 2:
    print("Usage: " + __file__ + " <h5file>")
    sys.exit(1)
extractimages(sys.argv[1])
