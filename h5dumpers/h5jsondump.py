#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
This script dumps a h5 in HTML. Like h5dump but HTML..
Usage: __file__ <h5file>
'''
import sys
import os
import h5py
import numpy
import h5todict
import json
from os import path
from pprint import pprint
from uuid import uuid4


class ObjectStrJSONEncoder(json.JSONEncoder):
    '''
    The default encoder will not attempt to stringify object. Override to do so.
    '''

    def default(self, o):
        '''
        Called when JSONEncoder has already failed.
        If this fails call JSONEncoder again to let it generate TypeError.
        '''
        value = None
        try:
            iterable = iter(o)
            value = list(iterable)
        except TypeError:
            value = str(o)
        if value is None:
            json.JSONEncoder.default(self, o)
        return value


def imageDataHandler(obj, context, depth):
    '''
    If dataset is an image output the image to file along side the data.
    Store the file's location in the image meta data.
    @todo Should be able to id the image uniquey via it's ref. Note sure what id.id actually is.
    '''
    try:
        if obj.attrs['CLASS'] == "IMAGE":
            imgFile = path.dirname(obj.file.filename)
            imgFile += "/.{0}.img.{1}.png".format(path.basename(obj.file.filename), str(obj.id.id))
            fOut = open(imgFile, "w")
            fOut.write(obj.value.tostring())
            fOut.close()
            context['filename'] = os.path.basename(imgFile)  # Relative to the h5.
    except KeyError:
        return False
    return True


if len(sys.argv) != 2:
    print "Usage: " + __file__ + " <h5file>"
    sys.exit(1)
# Load the HDF5.
h5File = sys.argv[1]
jsonFile = path.dirname(h5File) + "/." + path.basename(h5File) + ".meta.json"
h5 = h5py.File(h5File, "r")
fOut = open(jsonFile, "w")
h5Dict = h5todict.h5todict(h5, [imageDataHandler, h5todict.nullDataHandler])
h5Json = json.dumps(h5Dict, cls=ObjectStrJSONEncoder)
fOut.write(h5Json)
fOut.close()
print "Output to " + jsonFile
print "Dump:"
print h5Json
