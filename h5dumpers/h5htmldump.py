#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
This script dumps a h5 in HTML. Like h5dump but HTML..
Usage: __file__ <h5file>
'''
import sys
import os
import h5py
import numpy as np
from h5todict import h5todict
from os import path
from pprint import pprint
from jinja2 import Environment, FileSystemLoader, Template
tmpl = "templates/h5-generic.html"

# Load the HDF5.
# What meta do we need out of the h5? Just pass the whole thing over to the template..
h5 = h5py.File(sys.argv[1], "r")
h5Dict = h5todict(h5)
# pprint(h5Dict)
print Environment(loader=FileSystemLoader(path.dirname(tmpl))).get_template(path.basename(tmpl)).render({'h5': h5Dict})
