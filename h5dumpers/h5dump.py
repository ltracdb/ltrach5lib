#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Simple test script that tries to duplicate some of what h5dump does in python.
This script is not used or useful for anything.
'''
import sys
import os
import h5py
import numpy
from os import path
script = path.basename(__file__)

if len(sys.argv) != 2:
    print "Usage: " + script + " <h5file>"
    sys.exit(1)


def h5_visitor(obj, depth=0):
    '''
    Recursve function that prints out a h5 file's meta data.
    @input obj a Group or Dataset
    '''
    indent = "    "*depth
    if isinstance(obj, h5py.Group):
        print """{0}Group {1}:
{0}    id: {2}
{0}    ref: {3}""".format(indent, obj.name, obj.id, obj.ref)
        print "{0}    Attributes:".format(indent)
        for attr in obj.attrs:
            _attr = h5_get_attr(obj.attrs[attr])
            print "{0}        {1}: {2}".format(indent, attr, _attr)
        print "{0}    Items:".format(indent)
        for item in obj:
            h5_visitor(obj[item], depth+1)
    elif isinstance(obj, h5py.Dataset):
        print """{0}Dataset {1}:
        {0}    id: {id}
        {0}    ref: {ref}
        {0}    shape: {shape}
        {0}    dtype: {dtype}
        {0}    size: {size}""".format(
            indent,
            obj.name,
            id=obj.id,
            ref=obj.ref,
            shape=obj.shape,
            dtype=obj.dtype,
            size=obj.len()
        )
        print "{0}    Attributes:".format(indent)
        for attr in obj.attrs:
            _attr = h5_get_attr(obj.attrs[attr])
            print "{0}        {1}: {2}".format(indent, attr, _attr)


def h5_get_attr(attr):
    '''
    There is some weird ways of encoding a scalar about.
    We are not that worried about maintaining type, so try to guess if they meant `scalar`.
    '''
    _attr = attr
    if isinstance(attr, numpy.ndarray) and len(_attr) == 1:
        _attr = _attr[0]
        if isinstance(_attr, bytes):
            _attr = _attr.decode("utf-8")
    return _attr


h5 = h5py.File(sys.argv[1], "r")
print h5_visitor(h5)
