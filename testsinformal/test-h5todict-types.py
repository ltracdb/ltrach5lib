import os
import sys
import h5py
import numpy
from pprint import pprint
from h5todict import h5todict


h5 = h5py.File('/dev/null', 'w')
h5.attrs['a'] = 23
h5.attrs['b'] = '23'
h5.attrs['c'] = [1, 2, 3]
h5 = h5py.File('./datasets/misc/storm.h5')
dat = h5todict(h5)
pprint(dat)
pprint(type(dat['attributes']['fooindex']))
