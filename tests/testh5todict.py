import sys
import os
import h5py
import unittest
from pprint import pprint
from unittest import TestCase
from unittest.mock import MagicMock
from h5todict import h5todict


class test_scanner(TestCase):
    '''
    Test h5todict function.
    '''
    h5 = None

    def setUp(self):
        ''' Construct a simple H5 sufficing for all our simepl tests. '''
        self.h5 = h5py.File('/dev/null', 'w')
        self.h5.create_group('g1')
        self.h5.create_group('g2')
        self.h5.create_group('g3')
        self.h5.create_dataset('d1', (10,), 'i')
        self.h5.create_dataset('d2', (10, 10,), 'f')
        self.h5.create_dataset('d3', (10, 10, 10), 'i4')
        self.h5['g1'].create_group('g1')
        self.h5['g1'].create_dataset('d1', (10,), 'i')
        self.h5['g1'].create_dataset('d2', (10,), 'f')
        self.h5['g1'].attrs['myPath'] = 'g1'
        self.h5['g1/g1'].attrs['myPath'] = 'g1/g1'

    def test_h5todict(self):
        fhandler = MagicMock(return_value=False)
        handler = MagicMock(return_value=True)
        h5dict = h5todict(self.h5, [fhandler, handler])
        self.assertIsInstance(h5dict, dict)
        self.assertIsInstance(h5dict['groups']['g1'], dict)
        self.assertIsInstance(h5dict['groups']['g2'], dict)
        self.assertIsInstance(h5dict['groups']['g3'], dict)
        self.assertIsInstance(h5dict['groups']['g1']['groups']['g1'], dict)
        self.assertEqual(h5dict['groups']['g1']['attributes']['myPath'], 'g1')
        self.assertEqual(h5dict['groups']['g1']['groups']['g1']['attributes']['myPath'], 'g1/g1')
        d3 = h5dict['datasets']['d3']
        g1d2 = h5dict['groups']['g1']['datasets']['d2']
        self.assertIsInstance(d3, dict)
        self.assertEqual(d3['type'], 'dataset')
        self.assertEqual(d3['name'], '/d3')
        self.assertEqual(str(d3['ndim']), '3')
        self.assertEqual(d3['shape'], (10, 10, 10,))
        self.assertEqual(g1d2['dtype'], 'float32')
        self.assertEqual(fhandler.call_count, 5)
        self.assertEqual(handler.call_count, 5)

if __name__ == "__main__":
    unittest.main()
